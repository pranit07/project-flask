from pickle import TRUE
from flask import Flask, render_template
app = Flask(__name__, template_folder='template')

@app.route('/')
def home():
   return render_template('code.html')

   
@app.route('/frontpage')
def bootstrap():
   return render_template('bootstrap.html')
if __name__ == '__main__':
    
   app.run(debug=TRUE)